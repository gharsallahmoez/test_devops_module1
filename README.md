# Introduction
* In this module, we are going to build a web server with docker containers managed by Nginx reverse proxy that will act as the router between the containers.
* App1 contains docker-compose with Mysql server, alpine-nginx-php7 image running on port 8081, phpmyadmin running on port 8080 and SMTP on port 25
* App2 contains docker-compose with Mysql server, alpine-nginx-php7 image running on port 8082, phpmyadmin running on port 8083 and SMTP on port 26
* nginx reverse proxy run on port 80 and map App1 to localhost/app1 and App2 to localhost/app2

# Prerequisite 
* [Docker](https://docs.docker.com/get-docker/)
* for **windows**  you need to install [Make](http://gnuwin32.sourceforge.net/packages/make.htm)

# Configuration
* :warning: .env files are pushed for test purpose.
*  you can configure php.ini from `app1/config/php.ini` and `app2/config/php.ini`

# Build the project 
1. to build the project : 
```sh
make build
```
if you get *network with name module1_network already exists* error,  run `make remove-network`

2. open `localhost` or `127.0.0.1` on the browser.
3. click on App1 button to open App1 and App2 button to open App2.
4. here we tried to connect to mysql server from php7-nginx container. in case of success you will see the message `Connected to MYSQL successfully` along with the environment variables setted in docker-compose and the custome php.ini configuration. 
5. same for app2.
6. to stop the project : 
```sh
make stop
```
