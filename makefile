VERSION=1.0.0
NETWORK=module1_network
PROXY_IMAGE=proxy_img
PROXY_CONTAINER=proxy

build: create-network build-app1 build-app2 build-proxy

stop: stop-app1 stop-app2 stop-proxy remove-network 

build-app1:
	cd app1 && docker-compose up -d --build 

stop-app1:
	cd app1 && docker-compose down

build-app2:
	cd app2 && docker-compose up -d --build 

stop-app2:
	cd app2 && docker-compose down	

create-network:
	docker network create ${NETWORK}

remove-network:
	docker network remove ${NETWORK}

build-proxy:
	cd proxy && docker build -t ${PROXY_IMAGE} . && docker run -it -d -p 80:80 --network=${NETWORK} --name ${PROXY_CONTAINER} ${PROXY_IMAGE}

stop-proxy:
	cd proxy && docker rm -f ${PROXY_CONTAINER}	
